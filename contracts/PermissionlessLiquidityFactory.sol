// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IERC20.sol";

// This contract is inspired by the harberger tax idea, it rewards people with tokens for burning their liquidity provider
// tokens.
contract PermissionlessLiquidityFactory {

    // this represents a single recipient of token rewards on a fixed schedule that does not depend on deposit or burn rate
    // it specifies an id (key to a map below) an marker for the last time it was updated, a deposit (of LP tokens) and a
    // burn rate of those LP tokens per block, and finally, the owner of the slot, who will receive the rewards
    struct Slot {
        uint id;
        uint lastUpdatedBlock;
        uint vacatedBlock;
        uint depositWei;
        uint burnRateWei;
        address owner;
    }

    // rewardToken: the token that the rewards are made in
    // liquidityToken: the liquidity provider (LP) token
    // taxAddress: address to which taxes are sent
    struct Pool {
        uint id;
        address liquidityToken;
        address taxAddress;
        address poolOwner;
        uint maxStakers;
        uint minimumDepositWei;
        uint maximumDepositWei;
        uint minimumBurnRateWeiPerBlock;
        uint maximumBurnRateWeiPerBlock;
        mapping (uint => Slot) slots;
    }

    struct Metadata {
        bytes32 name;
        bytes32 ipfsHash;
    }

    struct Pulse {
        address rewardToken;
        uint rewardBalance;
        uint pulseStartBlock;
        uint pulseWavelengthBlocks;
        uint pulseAmplitudeWei;
        uint pulseIntegral;
        uint pulseConstant;
    }

    struct PoolStats {
        uint totalStakedWei;
        uint totalRewardsWei;
        uint totalBurnedWei;
        uint depositDecayWeiPerBlock; // decay only applies to vacated slots
        uint burnRateDecayWeiPerBlock; // decay only applies to vacated slots
        bool paused;
        uint pausedBlock;
        uint unpausedBlock;
        uint pausedStakers;
        uint numSynced; // only used while paused
        uint numStakers;
        mapping (address => uint) totalStakedWeiFor;
        mapping (address => uint) totalRewardsWeiFor;
        mapping (address => uint) totalBurnedWeiFor;
        mapping (uint => uint) rewardsWeiForSession;
    }

    uint public numPools;
    address public globalBeneficiary;
    uint public globalTaxPerCapita;

    mapping (uint => Pool) public pools;
    mapping (uint => Metadata) public metadatas;
    mapping (uint => PoolStats) public poolStats;
    mapping (uint => Pulse) public pulses;
    mapping (uint => uint) public globalTaxes;

    event SlotChangedHands(uint indexed poolId, address indexed newOwner, address indexed previousOwner, uint slotId, uint depositWei, uint burnRateWei, uint rewardsWeiForSession);
    event PoolAdded(uint indexed poolId, bytes32 indexed name, address indexed depositToken);

    modifier poolOwnerOnly(uint poolId) {
        Pool storage pool = pools[poolId];
        require (msg.sender == pool.poolOwner, 'Only pool owner may call this');
        _;
    }

    modifier initializedPoolOnly(uint poolId) {
        Pool storage pool = pools[poolId];
        require(pool.id == poolId && poolId > 0, 'Uninitialized pool');
        _;
    }

    modifier pausedAndSynced(uint poolId) {
        PoolStats storage stats = poolStats[poolId];
        require (stats.paused, 'Must be paused');
        require (stats.numSynced == stats.pausedStakers, 'Must sync all users');
        _;
    }

    constructor(address _globalBeneficiary, uint _globalTaxPerCapita) {
        globalBeneficiary = _globalBeneficiary;
        globalTaxPerCapita = _globalTaxPerCapita;
    }

    function addPool(
        address rewardTokenAddr,
        address liquidityTokenAddr,
        address taxAddr,
        address poolOwner,
        uint pulseStartDelayBlocks,
        bytes32 ipfsHash,
        bytes32 name) external {
        numPools++;
        {
            Pool storage pool = pools[numPools];
            pool.id = numPools;
            pool.liquidityToken = liquidityTokenAddr;
            pool.taxAddress = taxAddr;
            pool.poolOwner = poolOwner;
        }
        {
            Metadata storage metadata = metadatas[numPools];
            metadata.ipfsHash = ipfsHash;
            metadata.name = name;
        }
        {
            Pulse storage pulse = pulses[numPools];
            pulse.rewardToken = rewardTokenAddr;
            pulse.pulseStartBlock = block.number + pulseStartDelayBlocks;
        }
        {
            PoolStats storage stats = poolStats[numPools];
            stats.paused = true;
            stats.pausedBlock = block.number;
//            stats.pausedStakers = 0;
//            stats.unpausedBlock = 0;
        }
        emit PoolAdded(numPools, name, liquidityTokenAddr);
    }

    function fundPool(uint poolId, uint numPulses) public {
        Pool storage pool = pools[poolId];
        Pulse storage pulse = pulses[poolId];
        bool success = true;
        uint amount = numPulses * pulse.pulseIntegral * pool.maxStakers;
        require(IERC20(pulse.rewardToken).transferFrom(msg.sender, address(this), amount), 'Token transfer failed');
        pulse.rewardBalance += amount;
    }

    // compute the undistributed rewards for a slot
    function getRewards(uint poolId, uint slotId) internal view initializedPoolOnly(poolId) returns (uint) {
        {
            Slot storage slot = pools[poolId].slots[slotId];

            // unoccupied slots have no rewards
            if (slot.owner == address(0)) {
                return 0;
            }
        }

        (uint referenceBlock1, uint referenceBlock2) = getReferenceBlocks(poolId, slotId);

        // three parts, incomplete beginning, incomplete end and complete middle
        Pulse storage pulse = pulses[poolId];
        uint rewards;

        // complete middle
        // trim off overhang on both ends
        uint startPhase = (referenceBlock1 - pulse.pulseStartBlock) % pulse.pulseWavelengthBlocks;
        uint startOverhang = pulse.pulseWavelengthBlocks - startPhase;

        uint blocksDiffTotal = referenceBlock2 - referenceBlock1;

        uint endPhase = (referenceBlock2 - pulse.pulseStartBlock) % pulse.pulseWavelengthBlocks;
        uint endingBlocks = pulse.pulseWavelengthBlocks - endPhase;
        uint leftoverSum = pulseSum(pulse.pulseConstant, endingBlocks);
        // if we haven't made it to phase 0 yet
        if (blocksDiffTotal < startOverhang) {
            uint startSum = pulseSum(pulse.pulseConstant, startOverhang);
            rewards = startSum - leftoverSum;
        } else {
            uint blocksDiff = blocksDiffTotal - endPhase - startOverhang;
            uint waves = blocksDiff / pulse.pulseWavelengthBlocks;
            rewards = waves * pulse.pulseIntegral;

            // incomplete beginning of reward cycle, end of pulse
            if (startPhase > 0) {
                rewards += pulseSum(pulse.pulseConstant, startOverhang);
            }

            // incomplete ending of reward cycle, beginning of pulse
            if (endPhase > 0) {
                rewards += (pulse.pulseIntegral - leftoverSum);
            }
        }

        return rewards;
    }

    // compute the unapplied burn to the deposit
    function getBurn(uint poolId, uint slotId) public view initializedPoolOnly(poolId) returns (uint) {
        Slot storage slot = pools[poolId].slots[slotId];
        (uint referenceBlock1, uint referenceBlock2) = getReferenceBlocks(poolId, slotId);
        uint burn = slot.burnRateWei * (referenceBlock2 - referenceBlock1);
        if (burn > slot.depositWei) {
            burn = slot.depositWei;
        }
        return burn;
    }

    // this must be idempotent, it syncs both the rewards and the deposit burn atomically, and updates lastUpdatedBlock
    function updateSlot(uint poolId, uint slotId) public initializedPoolOnly(poolId) {
        Pool storage pool = pools[poolId];
        PoolStats storage stats = poolStats[poolId];
        Slot storage slot = pool.slots[slotId];
        require(slot.owner != address(0), 'Unoccupied slot');

        // prevent multiple updates on same slot while paused
        if (stats.paused) {
            // these two requires prevent weird double updates that might make numSynced too high
            // it also means that if someone updates you while paused you cannot withdraw...
            require(block.number > stats.pausedBlock, 'Do not call this in the same block that you paused');
            require(slot.lastUpdatedBlock <= stats.pausedBlock, 'If pool is paused, can only update slot once');
            require(msg.sender == pool.poolOwner || msg.sender == slot.owner, 'If pool is paused, only pool owner or slot owner may call this');
            stats.numSynced++;
        }

        Pulse storage pulse = pulses[poolId];
        uint rewards = getRewards(poolId, slotId);

        // burn and rewards always have to update together, since they both depend on lastUpdatedBlock
        uint burn = getBurn(poolId, slotId);

        // update this first to make burn and reward zero in the case of re-entrance
        // this must happen after getting rewards and burn since they depend on this var
        slot.lastUpdatedBlock = block.number;

        if (rewards > 0) {
            // bookkeeping
            stats.totalRewardsWei += rewards;
            stats.totalRewardsWeiFor[slot.owner] += rewards;
            stats.rewardsWeiForSession[slotId] += rewards;
            pulse.rewardBalance -= rewards;

            uint creatorTax = rewards * globalTaxPerCapita / 1000;
            uint transferAmount = rewards - creatorTax;
            globalTaxes[poolId] += creatorTax;

            // transfer the rewards
            require(IERC20(pulse.rewardToken).transfer(slot.owner, transferAmount), 'Token transfer failed');
        }

        if (burn > 0) {
            // adjust deposit first
            slot.depositWei -= burn;

            // bookkeeping
            stats.totalBurnedWei += burn;
            stats.totalBurnedWeiFor[slot.owner] += burn;

            require(IERC20(pool.liquidityToken).transfer(pool.taxAddress, burn), 'Token transfer failed');
        }
    }

    // most important function for users, allows them to start receiving rewards
    function claimSlot(uint poolId, uint slotId, uint newBurnRate, uint newDeposit) external {
        Pool storage pool = pools[poolId];
        PoolStats storage stats = poolStats[poolId];
        require(slotId > 0, 'Slot id must be positive');
        require(slotId <= pool.maxStakers, 'Slot id out of range');
        require(newBurnRate >= pool.minimumBurnRateWeiPerBlock, 'Burn rate must meet or exceed minimum');
        require(newBurnRate <= pool.maximumBurnRateWeiPerBlock, 'Burn rate must not exceed maximum');
        require(newDeposit >= pool.minimumDepositWei, 'Deposit must meet or exceed minimum');
        require(newDeposit <= pool.maximumDepositWei, 'Deposit must not exceed maximum');
        require(stats.paused == false, 'Must be unpaused');
        require(pool.id == poolId && poolId > 0, 'Uninitialized pool');
        {
            Pulse storage pulse = pulses[poolId];
            require(pulse.pulseStartBlock <= block.number, 'Pool has not started yet');
        }
        Slot storage slot = pool.slots[slotId];

        // count the stakers
        if (slot.owner == address(0)) {
            // assign id since this may be the first time
            slot.id = slotId;

            // set last updated block, this happens in updateSlot but that's the other branch
            slot.lastUpdatedBlock = block.number;

            // check that we meet-or-exceed the linearly-decayed deposit and burn rates
            (uint depositMin, uint burnRateMin) = getClaimMinimums(poolId, slotId);
            bool betterDeal = newBurnRate >= burnRateMin && newDeposit >= depositMin;
            require(betterDeal, 'You must meet or exceed the current burn rate and deposit');

            // increment counter
            stats.numStakers++;

        } else {
            updateSlot(poolId, slotId);

            //  this must go after updateSlot to sync the deposit variable
            bool betterDeal = newBurnRate > slot.burnRateWei && (newDeposit > slot.depositWei || newDeposit == pool.maximumDepositWei);
            require(betterDeal || slot.depositWei == 0, 'You must outbid the current owner');

            // bookkeeping
            stats.totalStakedWei -= slot.depositWei;
            stats.totalStakedWeiFor[slot.owner] -= slot.depositWei;

            // this is probably not necessary, but we do it to be tidy
            slot.vacatedBlock = 0;

            // if there's any deposit left,
            if (slot.depositWei > 0) {
                require(IERC20(pool.liquidityToken).transfer(slot.owner, slot.depositWei), 'Token transfer failed');
            }
        }

        emit SlotChangedHands(poolId, msg.sender, slot.owner, slotId, newDeposit, newBurnRate, stats.rewardsWeiForSession[slotId]);
        stats.rewardsWeiForSession[slotId] = 0;

        // set new owner, burn rate and deposit
        slot.owner = msg.sender;
        slot.burnRateWei = newBurnRate;
        slot.depositWei = newDeposit;

        // bookkeeping
        stats.totalStakedWei += newDeposit;
        stats.totalStakedWeiFor[msg.sender] += newDeposit;

        // transfer the tokens!
        if (newDeposit > 0) {
            require(IERC20(pool.liquidityToken).transferFrom(msg.sender, address(this), newDeposit), 'Token transfer failed');
        }
    }

    // separates user from slot, if either voluntary or delinquent
    function withdrawFromSlot(uint poolId, uint slotId) external initializedPoolOnly(poolId) {
        Pool storage pool = pools[poolId];

        PoolStats storage stats = poolStats[poolId];
        Slot storage slot = pool.slots[slotId];

        // prevent double-withdrawals
        require(slot.owner != address(0), 'Slot unoccupied');

        // sync deposit variable (this increments numSynced)
        updateSlot(poolId, slotId);

        // anyone can withdraw delinquents, but non-delinquents can only be withdrawn by themselves
        bool withdrawable = slot.owner == msg.sender || slot.depositWei == 0;
        require(withdrawable, 'Only owner can call this unless user is delinquent');

        // must do this before rewardsWeiForSession gets zeroed out
        emit SlotChangedHands(poolId, address(0), slot.owner, slotId, 0, 0, stats.rewardsWeiForSession[slotId]);
        stats.rewardsWeiForSession[slotId] = 0;

        // decrement the number of stakers
        stats.numStakers--;

        // record what block we vacated in to compute linear decay
        slot.vacatedBlock = block.number;

        // zero out owner, closing re-entrance gate
        address owner = slot.owner;
        slot.owner = address(0);

        // don't set deposit or burn rate to 0 so we can compute linear decay

        // if there's any deposit left,
        if (slot.depositWei > 0) {
            require(IERC20(pool.liquidityToken).transfer(owner, slot.depositWei), 'Token transfer failed');
        }
    }

    function withdrawTaxes(uint poolId) external initializedPoolOnly(poolId) {
        Pulse storage pulse = pulses[poolId];
        uint tax = globalTaxes[poolId];
        globalTaxes[poolId] = 0;
        bool success = IERC20(pulse.rewardToken).transfer(globalBeneficiary, tax);
        require(success, 'Token transfer failed');
    }

    // ======================== PAUSE =============================

    function pausePool(uint poolId) external poolOwnerOnly(poolId) initializedPoolOnly(poolId) {
        PoolStats storage stats = poolStats[poolId];
        require(stats.paused == false, 'Already paused');
        stats.paused = true;
        stats.pausedBlock = block.number;
        stats.pausedStakers = stats.numStakers;
        stats.unpausedBlock = 0;
    }

    function unpausePool(uint poolId) external poolOwnerOnly(poolId) pausedAndSynced(poolId) {
        PoolStats storage stats = poolStats[poolId];
        stats.paused = false;
        stats.pausedBlock = 0;
        stats.numSynced = 0;
        stats.pausedStakers = 0;
        stats.unpausedBlock = block.number;
    }

    // ======================== GETTERS =============================

    function getClaimMinimums(uint poolId, uint slotId) public view returns (uint, uint) {
        Slot memory slot = pools[poolId].slots[slotId];
        if (slot.owner != address(0)) {
            return (slot.depositWei, slot.burnRateWei);
        } else {
            PoolStats storage stats = poolStats[poolId];
            uint blocksDiff = block.number - slot.vacatedBlock;
            uint depositDecay = blocksDiff * stats.depositDecayWeiPerBlock;
            if (depositDecay > slot.depositWei) {
                depositDecay = slot.depositWei;
            }

            uint burnRateDecay = blocksDiff * stats.burnRateDecayWeiPerBlock;
            if (burnRateDecay > slot.burnRateWei) {
                burnRateDecay = slot.burnRateWei;
            }

            return (slot.depositWei - depositDecay, slot.burnRateWei - burnRateDecay);
        }
    }

    function getSlot(uint poolId, uint slotId) external view returns (uint, uint, uint, uint, uint, address) {
        Slot memory slot = pools[poolId].slots[slotId];
        PoolStats storage stats = poolStats[poolId];
        return (slot.lastUpdatedBlock, slot.depositWei, slot.burnRateWei, stats.rewardsWeiForSession[slotId], slot.vacatedBlock, slot.owner);
    }

    function getUserStats(uint poolId, address user) external view returns (uint, uint, uint) {
        PoolStats storage stats = poolStats[poolId];
        return (stats.totalStakedWeiFor[user], stats.totalRewardsWeiFor[user], stats.totalBurnedWeiFor[user]);
    }

    function getReferenceBlocks(uint poolId, uint slotId) internal view returns (uint, uint) {
        Pool storage pool = pools[poolId];
        PoolStats storage stats = poolStats[poolId];
        Slot memory slot = pool.slots[slotId];

        uint referenceBlock1 = slot.lastUpdatedBlock;
        uint referenceBlock2 = block.number;
        if (stats.paused) {
            referenceBlock2 = stats.pausedBlock;
        } else if (slot.lastUpdatedBlock < stats.unpausedBlock) {
            referenceBlock1 = stats.unpausedBlock;
        }

        return (referenceBlock1, referenceBlock2);
    }

    // compute the sum of the rewards per pulse
    function pulseSum(uint coeff, uint wavelength) public pure returns (uint) {
        // sum of squares formula
        return coeff * wavelength * (wavelength + 1) * ((2 * wavelength) + 1) / 6;
    }

    // ======================== SETTERS =============================

    function setConfig(
        uint poolId,
        uint newMaxStakers,
        uint newMinDeposit,
        uint newMaxDeposit,
        uint newMinBurnRate,
        uint newMaxBurnRate,
        uint newWavelength,
        uint newAmplitude) external poolOwnerOnly(poolId) pausedAndSynced(poolId) {
        Pool storage pool = pools[poolId];
        pool.maxStakers = newMaxStakers;
        pool.minimumDepositWei = newMinDeposit;
        pool.maximumDepositWei = newMaxDeposit;
        pool.minimumBurnRateWeiPerBlock = newMinBurnRate;
        pool.maximumBurnRateWeiPerBlock = newMaxBurnRate;

        Pulse storage pulse = pulses[poolId];
        pulse.pulseWavelengthBlocks = newWavelength;
        pulse.pulseAmplitudeWei = newAmplitude;
        pulse.pulseConstant = pulse.pulseAmplitudeWei / (pulse.pulseWavelengthBlocks * pulse.pulseWavelengthBlocks);
        pulse.pulseIntegral = pulseSum(pulse.pulseConstant, newWavelength);
    }

    // only management can change tax address
    function setTaxAddress(uint poolId, address newTaxAddress) poolOwnerOnly(poolId) external {
        Pool storage pool = pools[poolId];
        pool.taxAddress = newTaxAddress;
    }

    // only poolOwner can change pool owner key
    function setPoolOwner(uint poolId, address newOwner) poolOwnerOnly(poolId) external {
        Pool storage pool = pools[poolId];
        pool.poolOwner = newOwner;
    }

    function setDecays(uint poolId, uint burnRateDecayWeiPerBlock, uint depositDecayWeiPerBlock) external poolOwnerOnly(poolId) initializedPoolOnly(poolId)  {
        PoolStats storage stats = poolStats[poolId];
        stats.burnRateDecayWeiPerBlock = burnRateDecayWeiPerBlock;
        stats.depositDecayWeiPerBlock = depositDecayWeiPerBlock;
    }

    function setGlobalTax(uint newTax) external {
        require(msg.sender == globalBeneficiary, 'Only globalBeneficiary can change this');
        require(newTax < 1000, 'Tax too high');
        globalTaxPerCapita = newTax;
    }

}
